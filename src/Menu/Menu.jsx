import "./Menu.css";
import React from "react";
import Home from '../Home/Home'
import Products from '../Products/Products'
import {HashRouter,Route} from 'react-router-dom'


function template() {
  return (
    <div className="menu">
      <HashRouter>
      <div className="menu-items">
      <a href="#/home">Home</a>
      <a href="#/products">Products</a>
      
      
      </div>
      <div>
      <Route path='/home' component={Home}></Route>
      <Route path='/products' component={Products}></Route>
     
      </div>
      </HashRouter>
    </div>
  );
};

export default template;
