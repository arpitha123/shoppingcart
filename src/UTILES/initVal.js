import shoe from '../IMAGES/shoe.jpeg'
import suitcase from '../IMAGES/suitcase.jpeg'
import watch from '../IMAGES/watch.jpeg'
const initialVal = {
    currency:"INR",
    data: [
        {
            "p_name": "Shoe",
            "p_image": shoe,
            "p_cost": 85.00
        },
        {
            "p_name": "Suitcase",
            "p_image": suitcase,
            "p_cost": 15.00
        },
        {
            "p_name": "Watch",
            "p_image": watch,
            "p_cost": 25.00
        }
    ]
}
export default initialVal;