import React    from "react";
import template from "./Productitems.jsx";

class Productitems extends React.Component {
  render() {
    return template.call(this);
  }
}

export default Productitems;
