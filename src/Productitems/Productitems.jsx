import "./Productitems.css";
import React from "react";

function template() {
  const { p_name, p_image, p_cost } = this.props.Product;
  return (
    <div className="productitems">
      <img src={p_image} />
      <p>{p_name}</p>
      <p>{p_cost}&#8377;</p>

    </div>

  );
};

export default template;
