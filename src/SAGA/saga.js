import {takeLatest,put,call} from 'redux-saga/effects'
import ServerCall from '../serverCall';

function* fnGetData(data){
    debugger;
 let res=yield call(ServerCall.fnGetReq,'latest?base='+data.curr);
   let prevCurrVal =res.data.rates[data.currency];
   let singleVal=1/prevCurrVal;
   let updateData=data.CurrentData.map((obj)=>{
       obj.p_cost=Math.round(obj.p_cost*singleVal)
       return obj;
   })
   yield put({

       type:"UPDATE",
       data:updateData,
       currency:data.curr
   })
 
}
function* saga(){
    takeLatest("GET-DATA",fnGetData)
}
export default saga;