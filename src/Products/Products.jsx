import "./Products.css";
import React from "react";
import Productitems from '../Productitems/Productitems'

function template() {
  const { data } = this.props;
  return (
    <div className="container-fluid">
    <div className="products row form-group">
      <div className='col-sm-9'>
      {
        data.map((obj, i) => {
          return <Productitems Product={obj} key={i} />
        })
      }
      </div>
      <div className='col-sm-2 text-right'>Currency:</div>
      <div className='col-sm-1 text-left'>
      <select onChange={this.fnCurrencyChange.bind(this)}>
        <option>INR</option>
        <option>USD</option>
      </select>
      </div>
      
    </div>
    </div>
  );
};

export default template;
