import {createStore,applyMiddleware,combineReducers} from 'redux'
import logger from 'redux-logger'
import reducer from '../REDUCER/reducer'
import saga from '../SAGA/saga'
import createSagaMiddleware from 'redux-saga'
const sagaMiddleware=createSagaMiddleware();
const store=createStore(combineReducers({reducer}),applyMiddleware(logger,sagaMiddleware));
sagaMiddleware.run(saga);
export default store;