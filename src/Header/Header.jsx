import "./Header.css";
import React from "react";

function template() {
  return (
    <div className="header">
      <header className='text-primary'>Online Shopping Cart</header>
    </div>
  );
};

export default template;
